// $Id$

/**
 * Controls the behavior of the manual textfield.
 */
Drupal.behaviors.druOcrManualTitle = function (context) {
  // If title source is not manual hide the textfield
  if($("input[name=ocr_for_drupal_title_source]:checked").val() != 2) {
    $("#edit-ocr-for-drupal-title-manual-wrapper").hide();
  }

  // Toggle the manual textfield 
  $("input[name=ocr_for_drupal_title_source]").bind('click', function () {
    if($(this).val() == 2) {
      $("#edit-ocr-for-drupal-title-manual-wrapper").show();
    }
    else {
      $("#edit-ocr-for-drupal-title-manual-wrapper").hide();
    }
  });
  
  // Validate manual textfield
  $('#ocr-for-drupal-settings-form').bind('submit', function () {
    var spaces = new RegExp("^\\s*$");
    if($("input[name=ocr_for_drupal_title_source]:checked").val() == 2 && spaces.test($('input[name=ocr_for_drupal_title_manual]').val())) {
      $('#edit-ocr-for-drupal-title-manual').next('.description').css('color', 'red');
      return false;
    }
  });
};
