// $Id$

OCR for Drupal
------------------------
By: Krishnaprasad MG (http://www.sunspikes.com)

This module creates nodes using text extracted from images. 
The module uses tesseract for extraction process

The extraction process can be done,
1. Interactively: In this way the user can choose an image file, 
   this will take the use to a node creation page, where user will be provided 
   with prepopulated body field, and an empty title fild and an option to choose 
   content type for the node.
2. As batch: That is the user can choose a particular directory inside files folder 
   and the module will create nodes as batch with text extracted from 
   image as body and as of now the filename as title and content type 
   given in the admin settings.

Installation Instructions:
1. Install Tesseract in the target system. 
(instructions at http://code.google.com/p/tesseract-ocr)
2. Copy the 'OCR for Drupal' module to your module directory and enable it per usual.
4. Navigate to admin/build/ocr_for_drupal,
 i) Choose the tab 'Import images' and upload the file and follow the instructions to 
    import text from images interactively.
 ii) Choose the tab 'Batch Import Images' if you want to import all the images in a folder
